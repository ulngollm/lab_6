<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <title>Лабораторная работа</title>
    <link rel="stylesheet" type="text/css" href="/views/css/style.css">
</head>

<body>
    <div id="wrapper">
        <div id="header">
            <div class="header__title">Лабораторная работа</div>
            <div class="header__user-block">
                {AUTH}
            </div>
        </div>
        <div id="menu">
            {MENU}
        </div>
        <div id="content">
            {CONTENT}
            <div id="pagination">
                {PAGINATION}
            </div>
        </div>

        <div id="footer">Донской Государственный Технический Университет <br> ully</div>
    </div>
</body>

</html>