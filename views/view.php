<?php
require_once ROOT . '/components/menu.php';
include_once ROOT . '/components/template.php';
include_once ROOT . '/views/formView.php';

class MainView
{
    public static function init($currentSection = '/', $isAuth = false, string $authError = "", string $template = '/views/templates/index.tpl')
    {
        if(!empty($_SESSION)){
            (empty($_SESSION['auth_error']))? $isAuth = true: $authError = $_SESSION['auth_error'];
        }
        $pageTemplate = new Template(ROOT . $template);
        $menu = new Menu($currentSection);
        if ($isAuth) {
            $auth = FormView::UserInfo();
        } else $auth = FormView::Auth($authError);

        $pageTemplate->SetValue('MENU', $menu->GetMenu());
        $pageTemplate->SetValue('AUTH', $auth);
        return $pageTemplate;
    }
}
