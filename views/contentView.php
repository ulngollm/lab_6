<?php
require_once ROOT . '/components/pagination.php';
include_once ROOT . '/views/view.php';

class ContentView
{
    private $data;
    private $viewType; //list or detail
    private $template;
    private $pagination;
    private $section;

    public function __construct(array $sqlResult, string $viewType, string $section, int $currentPage = 0, int $rowsCount = 0 )
    {
        $this->data = $sqlResult;
        $this->viewType = $viewType;
        $this->section = $section;
        $this->rowsCount = $rowsCount;
        $this->template = MainView::init($section);
        $baseLink = "/$section/list";
        if($viewType == "list") $this->pagination = new Pagination($rowsCount, $currentPage, $baseLink);
    }

    private function listView()
    {
        $table = Template::FormatResult($this->data, $this->section);
       
        $this->template->SetValue('CONTENT', $table);
        $this->template->SetValue('PAGINATION', $this->pagination->GetPagination());
        print($this->template->ToString());        

    }
    private function detailView()
    {
        $table = Template::FormatResult($this->data, $this->section, false);
        $this->template->SetValue('CONTENT', $table);
        $this->template->SetValue('PAGINATION', "<a class=\"link\" href=\"/$this->section/list/1\">Назад</a>");
        print($this->template->ToString());
    }
    public function render()
    {
        $renderTpl = "{$this->viewType}View";
        $this->$renderTpl();
    }
}

