<?php
include_once ROOT . '/views/view.php';
class FormView
{
    private $template;
    private $message;

    public function __construct(string $message)
    {
        $this->template = MainView::init();
        $this->message = $message;
    }
    public function getRegistrationForm()
    {
        $this->template->SetValue('AUTH', '');
        if ($this->message != 'OK')
            $this->template->SetValue('CONTENT', self::Signup($this->message));
        else $this->template->SetValue('CONTENT', 'Вы успешно зарегистрированы');
        $this->template->SetValue('PAGINATION', '');

        print($this->template->ToString());
    }
    //только для неавторизованного пользователя или при входе с ошибкой
    public static function Auth(string $errorMessage = "")
    {
        $templatePath = ROOT . "/views/templates/header_form_auth.html";
        $auth = new Template($templatePath);
        $auth->SetValue("ERROR", $errorMessage);
        unset($_SESSION['auth_error']);
        return $auth->ToString();
    }
    //если перешел на страницу регистрации
    public static function Signup(string $errorMessage = "")
    {
        $templatePath = ROOT . "/views/templates/form.html";
        $form = new Template($templatePath);
        $form->SetValue("ERROR", $errorMessage);
        return $form->ToString();
    }
    public static function UserInfo()
    {
        $templatePath = ROOT . '/views/templates/header_user_block.html';
        $userInfo = new Template($templatePath);
        $userInfo->SetValue("NAME", $_SESSION['name']);
        $userInfo->SetValue('EMAIL', $_SESSION['mail']);
        return $userInfo->ToString();
    }
    public function render()
    {
        $this->getRegistrationForm();
    }
}
