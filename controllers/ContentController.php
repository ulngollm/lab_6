<?php
require_once ROOT . '/models/ContentData.php';
require_once ROOT . '/views/contentView.php';

class ContentController
{
    private $structure;
    public function __construct()
    {
        $this->structure = include_once(ROOT. '/config/tables.php');
    }
    public function actionList($section, $currentPage)
    {
    
        $contractorList = array();
        $contractorList = ContentData::getElementList($rowsCount, (int)$currentPage, $this->structure[$section]);
        $view = new ContentView($contractorList, "list", $section, (int)$currentPage, $rowsCount);
        $view->render();
        return true;
    }
    public function actionDetail($section, $id)
    {
        if ($id) {
            $contractor = ContentData::getElement($id, $this->structure[$section]);
            $view = new ContentView($contractor, "detail", $section);
            $view->render();
        }
        return true;
    }
}
