<?php
include_once ROOT . '/models/UserData.php';
include_once ROOT . '/views/formView.php';
class UserController
{
    private $viewType;
    public function __construct()
    {
    }
    public function actionSignup()
    {
        $message = '';
        if ($_REQUEST) {
            $name = htmlspecialchars($_REQUEST['name']);
            $mail = htmlspecialchars($_REQUEST['mail']);
            $password = md5(htmlspecialchars($_REQUEST['password']));
            $signupResult = UserData::addUser($name, $mail, $password);
            if($signupResult) $_SESSION['auth'] = true;
            $message = ($signupResult)? 'OK': "Пользователь $name уже существует!";
            
        }
        $view = new FormView($message);
        $view->render();
    }
    public function actionAuth()
    {
        if($_REQUEST){
            unset($_SESSION['auth_error']);
            $mail = htmlspecialchars($_REQUEST['mail']);
            $password = htmlspecialchars($_REQUEST['password']);
            $_SESSION = UserData::authUser($mail, $password, $error);
            if($error) $_SESSION['auth_error'] = $error;
            else $_SESSION['auth'] = true;
        }   
        header("Location: /");
    }
    public function actionExit()
    {
        session_destroy();
        unset($_SESSION['name']);
        unset($_SESSION['mail']);
        header("Location: /");
    }
}
