<?php

class Template
{

    public $values = array();
    public $html;

    public function __construct($tpl_name)
    {
        if(file_exists($tpl_name)){
            $this->html = file_get_contents($tpl_name);
        }
        else die("Не найден файл шаблона $tpl_name");
    }
    public static function FormatResult(array $sqlResult, $section = '', $isList = true)
    {
        $flagfirst = true;

        $result = "<table><thead>";
        foreach($sqlResult as $row) {
            if ($flagfirst) {
                $key = array_keys($row);
                $result .= "<tr>";
                for ($i = 0; $i < count($key); $i++) {
                    $result .= "<th>$key[$i]</th>";
                }
                if($isList) $result.= "<th></th>";
                $flagfirst = false;
                $result .= "</tr></thead><tbody>";
            }
            $result .= '<tr>';
            foreach($row as $key => $value){
                $result .=  "<td>$value</td>"; 
            }
            if($isList) $result .= "<td><a class=\"link\" href=\"/$section/$row[Code]\">...</a></td>";
            $result .= "</tr>";
        }
        $result .= "</tbody></table>";
        return $result;
    }

    public function SetValue($key, $var)
    {
        $this->values["{{$key}}"] =  $var;
    }

    private function TemplateParse()
    {
        foreach($this->values as $entry => $replacer){
            $this->html = str_replace($entry, $replacer, $this->html);
        }
    }

    public function ToString(){
        $this->TemplateParse();
        return $this->html;
    }
}