<?php

class Pagination
{
    public static int $perPage = 5;
    public $currentPage;
    public $maxPages = 6;
    public $pagesAround;
    public $pageCount;
    public $lowerPoint;
    public $upperPoint;
    public $basicLink;



    public function __construct(int $rowsCount, int $currentPage, string $basicLink)
    {
        $this->pageCount = $this->getPageCount($rowsCount);
        $this->currentPage = (int)$currentPage;
        $this->pagesAround = $this->maxPages / 2;
        $this->lowerPoint = max(($currentPage - $this->pagesAround), 1);
        $this->upperPoint = min(($currentPage + $this->pagesAround), $this->pageCount);
        $this->basicLink = $basicLink;
    }

    public function getPageCount($rowsCount)
    {
        return ceil($rowsCount / self::$perPage);
    }

    public function GetLink(int $index)
    {
        return ($index == $this->currentPage) ? "<span class=\"link link_active\">$index</span>" : "<a href=\"$this->basicLink/$index\" class=\"link\">$index</a>";
    }
    public function GetLinksRange()
    {
        $result = '';
        for ($i = $this->lowerPoint; $i <= $this->upperPoint; $i++) {
            $result .= $this->GetLink($i);
        }
        return $result;
    }
    public function GetPagination()
    {
        $pagination = ''; 
        if($this->pageCount > $this->maxPages + 1){
            if ($this->lowerPoint == 1) $this->upperPoint = $this->maxPages + 1;
            if ($this->upperPoint == $this->pageCount) $this->lowerPoint = $this->pageCount - $this->maxPages - 1;
        }
        if ($this->lowerPoint != 1) {
            $pagination .= $this->setFirstLinkPoint();
        }
        $pagination .= $this->GetLinksRange();
        if ($this->upperPoint != $this->pageCount) $pagination .= $this->setLastLinkPoint();
        return $pagination;
    }
    public function setFirstLinkPoint()
    {
        return "<a href=\"$this->basicLink/1\" class=\"link\">1</a><span>...</span>";
    }
    public function setLastLinkPoint()
    {
        return "<span>...</span><a href=\"$this->basicLink/$this->pageCount\" class=\"link\">$this->pageCount</a>";
    }
}
