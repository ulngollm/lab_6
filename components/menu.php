<?php

class Menu{
    public $itemList;
    public $currentSection;
    public function __construct($currentSection)
    {
        $this->itemList = include(ROOT.'/config/section.php');
        $this->currentSection = $currentSection;
    }
    public function GetMenu(){
        $menu = '';
        foreach ($this->itemList as $code=>$name){
            if($code != $this->currentSection)
            $menu .= "<a class=\"link\" href=\"/$code/list/1\">$name</a>";
            else $menu .= "<a class=\"link link_active\" href=\"/$code/list/1\">$name</a>";
        }
        return $menu;
    }
}