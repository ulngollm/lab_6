<?php
include_once ROOT.'/components/pagination.php';
require_once ROOT.'/components/safemysql.php';

class ContentData
{
    public static function getElement($id, $table = "ContractingParties")
    {
        $db = new SafeMySQL();

        $query = "SELECT * FROM ?n WHERE code=?s";
        return $db->getAll($query, $table, $id);
    }
    private static function getTotalRowsCount($db){
        return $db->getOne("SELECT FOUND_ROWS()");
    }
    public static function getElementList(&$rowsCount, int $pageNum = 1, $table = "ContractingParties")
    {
        $db = new SafeMySQL();

        $query = "SELECT SQL_CALC_FOUND_ROWS * FROM ?n LIMIT ?i OFFSET ?i";
       
        $count = Pagination::$perPage;
        $offset = ($pageNum - 1) * $count;
        
        $result = $db->getAll($query, $table, $count, $offset);
        $rowsCount = self::getTotalRowsCount($db);
        return $result;
    }
}
