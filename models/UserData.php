<?php
require_once ROOT.'/components/safemysql.php';

class UserData
{
    public static function addUser($name, $mail, $password)
    {
        $db = new SafeMySQL();
        if (!self::isUserExists($mail, $db)) {
            $db->query("INSERT INTO user(name, mail, password, date) VALUES(?s, ?s,?s, CURRENT_DATE())", $name, $mail, $password);
            return true;
        } else return false;
    }

    public static function isUserExists($email, $db)
    {
        $result = $db->getOne("SELECT * from ?n where mail=?s", 'user', $email);
        return $result;
    }

    public static function authUser($mail, $password, &$error)
    {
        $db = new SafeMySQL();
        $result = $db->getRow("SELECT mail, name, password from ?n where mail=?s", 'user', $mail);
        $db_password = array_pop($result);
        if (empty($result)) {
            $error = "Пользователь $mail не найден";
            return false;
        } else {
            if ($db_password != md5($password)) {
                $error = 'Неверный пароль';
                return false;
            } else {
                return $result; 
            }
        };
    }
}
